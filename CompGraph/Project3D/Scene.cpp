#include "Scene.h"
#include "Instance.h"
#include "Gizmos.h"

Scene::Scene(Camera* camera, glm::vec2 windowSize, Light& light, glm::vec3 ambientLight)
{
	m_camera = camera;
	m_windowSize = windowSize;
	m_sunLight = light;
	m_ambientLight = ambientLight;
}

Scene::~Scene()
{
	for (auto it = m_instances.begin(); it != m_instances.end(); it++)
		delete* it;
}

void Scene::addInstance(Instance* instance)
{
	m_instances.push_back(instance);
}

void Scene::removeInstance(int index)
{
	if (index < m_instances.size())
	{
		m_instances.erase(m_instances.begin() + index);
	}
}

void Scene::addLight(Light light)
{
	m_pointLights.push_back(light);
}

void Scene::removeLight(int index)
{
	if (index < m_pointLights.size())
	{
		m_pointLights.erase(m_pointLights.begin() + index);
	}
}

void Scene::draw()
{
	for (int i = 0; i < m_pointLights.size(); i++)
	{
		if (!m_pointLights[i].drawHidden)
		{
			//Add way to indicate light isnt used?
			glm::vec3 tempColour = m_pointLights[i].rawColour; //glm::normalize(m_pointLights[i].getRawColour());

			aie::Gizmos::addSphere(m_pointLights[i].direction, 0.25f, 10, 10, { tempColour.r, tempColour.g, tempColour.b,1 });
		}
	}

	//clear lights so we can add and remove or disable them without worry
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		m_pointLightPositions[i] = { 0,0,0 };
		m_pointLightColours[i] = { 0,0,0 };
	}
	int activeLights = 0;
	for (int i = 0; activeLights < MAX_LIGHTS && i < m_pointLights.size(); i++)
	{
		if (!m_pointLights[i].lightHidden)
		{
			m_pointLightPositions[activeLights] = m_pointLights[i].direction;
			m_pointLightColours[activeLights] = m_pointLights[i].getColour();
			activeLights++;
		}
	}

	for (int i = 0; i < m_instances.size(); i++)
	{
		m_instances[i]->draw(this, /*check if previous instance has the same shader*/i >= 1 && m_instances[i]->getShader() == m_instances[i - 1]->getShader());
	}
}