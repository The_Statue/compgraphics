#pragma once
#include "Camera.h"
#include <glm/glm.hpp>
#include <list>
#include <vector>

struct Light {
	Light()
	{
		direction = { 0,1,0 };
		rawColour = { 1,1,1 };
		intensity = 1;
	}
	Light(glm::vec3 pos, glm::vec3 col, float inten)
	{
		direction = pos;
		rawColour = col;
		intensity = inten;
	}
	glm::vec3 direction;
	glm::vec3 getColour() { return rawColour * intensity; }

	glm::vec3 rawColour;
	float intensity;

	bool drawHidden = false;
	bool lightHidden = false;
};

class Instance;

class Scene
{
public:
	Scene(Camera* camera, glm::vec2 windowSize, Light& light, glm::vec3
		ambientLight);
	~Scene();
	void addInstance(Instance* instance);
	void removeInstance(int index);
	void addLight(Light light);
	void removeLight(int index);
	void draw();

	Camera* getCamera() { return m_camera; }
	glm::vec2 getWindowSize() { return m_windowSize; }
	Light getLight() { return m_sunLight; }
	glm::vec3 getAmbientLight() { return m_ambientLight; }

	int getNumLights() { return (int)m_pointLights.size(); }
	glm::vec3* getPointlightPositions() { return &m_pointLightPositions[0]; }
	glm::vec3* getPointlightColours() { return &m_pointLightColours[0]; }
	std::vector<Light>& getPointLights() { return m_pointLights; }

	std::vector<Instance*> getInstances() { return m_instances; }
protected:
	Camera* m_camera;
	glm::vec2 m_windowSize;
	Light m_sunLight;
	std::vector<Light> m_pointLights;
	glm::vec3 m_ambientLight;
	std::vector<Instance*> m_instances;

	static const int MAX_LIGHTS = 4; //keep same as normalmap.frag
	glm::vec3 m_pointLightPositions[MAX_LIGHTS];
	glm::vec3 m_pointLightColours[MAX_LIGHTS];
};