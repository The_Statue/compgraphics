#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "Camera.h"
#include "Instance.h"
#include "Scene.h"

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	float random(float min, float max);

protected:

	bool m_menuActive = false;
	int m_menuSelected = 0;

	//float cameraRotation = 0;
	//float cameraZoom = 10;
	//float cameraHeight = 1;
	//glm::mat4	m_viewMatrix;
	//glm::mat4	m_projectionMatrix;
	Camera	m_camera;
	float m_cameraSpeed = 5;

	aie::Texture m_gridTexture;

	//aie::ShaderProgram m_shader;
	aie::ShaderProgram m_texturedShader;
	//aie::ShaderProgram m_phongShader;
	aie::ShaderProgram m_normalMapShader;

	//struct Object {
	//	aie::OBJMesh*  mesh;
	////	aie::ShaderProgram* m_shader;
	//	glm::mat4   transform;
	//	bool assigned = false;
	//	~Object() 
	//	{
	//	}
	//};

	//const static int m_objectCount = 26;
	//Object m_objects [m_objectCount] ;


	Mesh     m_quadMesh;
	glm::mat4   m_quadTransform;

	aie::OBJMesh  m_spearMesh;
	Instance* m_spearInstance;
	//Light  m_light;
	Scene* m_scene;

	int m_lightChoice = 0; //which light to edit

	//aie::OBJMesh  m_bunnyMesh;
	//glm::mat4   m_bunnyTransform;

	//glm::mat4   m_spearTransform;

	glm::vec3   m_ambientLight;
	glm::vec3   m_specularLight;
	float m_specular = 0;

	int m_distribution[488] = { 0 };

	int objectIndex = 0; //used for fancy animation shenanigans
	bool followObjectAnim = false; //used for fancy animation shenanigans
};