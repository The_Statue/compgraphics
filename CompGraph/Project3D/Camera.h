#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{
public:

	void update(float deltaTime, float camSpeed);
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix(float w, float h);
	glm::vec3 getPosition() { return m_position; }
	void setPosition(glm::vec3 pos) { m_position = pos; }
private:
	float m_theta = -90;
	float m_phi = 0;
	glm::vec3 m_position = { 0, 1.9, 1.2 };


	float m_lastMouseX;
	float m_lastMouseY;
};