#pragma once
#include "OBJMesh.h"
#include "Shader.h"
#include <glm/glm.hpp>
#include "Camera.h"

//
class Scene;

class Instance
{
public:
	Instance(aie::OBJMesh* mesh, aie::ShaderProgram* shader, glm::vec3 position = { 0,0,0 }, glm::vec3 rotation = { 0,0,0 }, glm::vec3 scale = { 1,1,1 });
	void draw(Scene* scene, bool prebinded);
	//void draw(Camera* camera, float windowWidth, float windowHeight, glm::vec3 ambientLight, Light* light);
	glm::mat4 makeTransform(glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale);

	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
	
	aie::ShaderProgram* getShader() { return m_shader; }
protected:
	glm::mat4 m_transform;
	aie::OBJMesh* m_mesh;
	aie::ShaderProgram* m_shader;
};
