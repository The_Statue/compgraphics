#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

Application3D::Application3D() {

}

Application3D::~Application3D() {

}

bool Application3D::startup() {

	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	//m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	//m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.f);


	//m_light.colour = { 1, 1, 1 };
	//m_light.direction = { 0.1,-1,-0.1 };
	//m_ambientLight = { 0.25f, 0.25f, 0.25f };

	// load imaginary texture 
	aie::Texture texture1;
	texture1.load("mytexture.png");

	// create a 2x2 black-n-white checker texture 
   // RED simply means one colour channel, i.e. grayscale 
	aie::Texture texture2;
	unsigned char texelData[4] = { 0, 255, 255, 0 };
	texture2.create(2, 2, aie::Texture::RED, texelData);

	//m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/simple.vert");
	//m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/simple.frag");
	//if (m_shader.link() == false) {
	//	printf("Shader Error: %s\n", m_shader.getLastError());
	//	return false;
	//}
	//m_phongShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");
	//m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	//if (m_phongShader.link() == false) {
	//	printf("Shader Error: %s\n", m_phongShader.getLastError());
	//	return false;
	//}

	m_texturedShader.loadShader(aie::eShaderStage::VERTEX,
		"./shaders/textured.vert");
	m_texturedShader.loadShader(aie::eShaderStage::FRAGMENT,
		"./shaders/textured.frag");
	if (m_texturedShader.link() == false) {
		printf("Shader Error: %s\n",
			m_texturedShader.getLastError());
		return false;
	}
	m_normalMapShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/normalmap.vert");
	m_normalMapShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/normalmap.frag");
	if (m_normalMapShader.link() == false) {
		printf("Shader Error: %s\n", m_normalMapShader.getLastError());
		return false;
	}

	if (m_gridTexture.load("./textures/numbered_grid.tga") == false) {
		printf("Failed to load texture!\n");
		return false;
	}

	m_quadMesh.initialiseQuad();

	//make the quad 10 units wide 
	m_quadTransform = {
	 10,0,0,0,
	 0,10,0,0,
	 0,0,10,0,
	 0,0,0,1 };

	if (m_spearMesh.load("./soulspear/soulspear.obj",
		true, true) == false) {
		printf("Soulspear Mesh Error!\n");
		return false;
	}

	Light light(vec3(5, 3, 0), vec3(1, 1, 1), 1);

	m_scene = new Scene(&m_camera, glm::vec2(getWindowWidth(), getWindowHeight()), light, glm::vec3(0.25f, 0.25f, 0.25f));

	// red light on the left 
	m_scene->addLight(Light(vec3(5, 3, 0), vec3(1, 0, 0), 50));
	// green light on the right 
	m_scene->addLight(Light(vec3(-5, 3, 0), vec3(0, 1, 0), 50));


	int count = 200;
	for (int i = 0; i < count; i++)
	{
		m_scene->addInstance(new Instance(&m_spearMesh, &m_normalMapShader, { 0,0,0 }, { 0,0,0 }, { 0.1,0.1,0.1 }));
	}
	return true;
}

void Application3D::shutdown() {

	Gizmos::destroy();
	delete m_scene;
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	int index = (((int)floor(random(-66.6, 420.69)) + 67) * 1);
	m_distribution[index]++;

	// rotate camera
	m_camera.update(deltaTime, m_cameraSpeed);
	//m_viewMatrix = glm::lookAt(vec3(glm::sin(cameraRotation) * cameraZoom, 0/*Angle of cam*/ + cameraHeight, glm::cos(cameraRotation) * cameraZoom),
	//	vec3(0, cameraHeight, 0), vec3(0, 1, 0));

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
			vec3(-10 + i, 0, -10),
			i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
			vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	//Gizmos::addTransform(mat4(1));


	ImGui::Begin("Editor Settings");
	//ImGui::DragFloat3("Sunlight Direction", &m_light.direction[0], 0.1f, -1.0f, 1.0f);
	//ImGui::DragFloat3("Sunlight Colour", &m_light.colour[0], 0.1f, 0.0f, 2.0f);
	//ImGui::DragFloat3("Specular Colour", &m_specularLight[0], 0.1f, 0.0f, 1.0f);

	//if (ImGui::CollapsingHeader("Light options"))
	//{
	//	ImGui::DragInt("Light selection", &m_lightChoice, 0.1f, 0, m_scene->getPointLights().size() - 1);
	//	ImGui::DragFloat3("Light Position", &m_scene->getPointLights()[m_lightChoice].direction[0], 0.1f, -100.0f, 100.0f);
	//	ImGui::ColorEdit3("Light Color", &m_scene->getPointLights()[m_lightChoice].rawColour[0]);
	//	ImGui::DragFloat("Light Intensity", &m_scene->getPointLights()[m_lightChoice].intensity, 0.1f, 0, 100);
	//}

	ImGui::Text("Current FPS:");

	char num_char[10 + sizeof(char)];
	sprintf(num_char, "%d", getFPS());

	ImGui::SameLine();
	ImGui::Text(num_char);

	if (ImGui::TreeNode("Scene"))
	{
		if (ImGui::TreeNode("Lights"))
		{
			for (int i = 0; i < m_scene->getPointLights().size(); i++)
			{
				if (ImGui::TreeNode((void*)(intptr_t)i, "Light %d", i))
				{
					ImGui::DragFloat3("Light Position", &m_scene->getPointLights()[i].direction[0], 0.1f, -100.0f, 100.0f);
					ImGui::ColorEdit3("Light Color", &m_scene->getPointLights()[i].rawColour[0]);
					ImGui::DragFloat("Light Intensity", &m_scene->getPointLights()[i].intensity, 0.1f, 0, 100);

					if (ImGui::Button("Hide sphere"))
						m_scene->getPointLights()[i].drawHidden = !m_scene->getPointLights()[i].drawHidden;
					ImGui::SameLine();
					if (ImGui::Button("Hide light"))
						m_scene->getPointLights()[i].lightHidden = !m_scene->getPointLights()[i].lightHidden;
					ImGui::SameLine();
					if (ImGui::Button("Remove light"))
						m_scene->removeLight(i);

					ImGui::TreePop();
				}
			}
			if (ImGui::Button("Add new light"))
				m_scene->addLight(Light(m_scene->getCamera()->getPosition(), vec3(1, 1, 1), 5));

			ImGui::TreePop();
		}
		if (ImGui::TreeNode("Objects"))
		{
			for (int i = 0; i < m_scene->getInstances().size(); i++)
			{
				if (ImGui::TreeNode((void*)(intptr_t)i, "Object %d", i))
				{

					glm::vec3* position = &m_scene->getInstances()[i]->m_position;
					glm::vec3* rotation = &m_scene->getInstances()[i]->m_rotation;
					glm::vec3* scale = &m_scene->getInstances()[i]->m_scale;


					ImGui::DragFloat3("Object Position", &position[0][0], 0.1f, -100.0f, 100.0f);
					ImGui::DragFloat3("Object Rotation", &rotation[0][0], 0.1f, -360.0f, 360.0f);
					ImGui::DragFloat3("Object Scale", &scale[0][0], 0.1f, -100.0f, 100.0f);

					//m_scene->getInstances()[i]->m_transform = m_scene->getInstances()[i]->makeTransform(position, rotation, scale);
					ImGui::TreePop();
				}
				else
					ImGui::SameLine();
				if (ImGui::Button("Remove Object"))
					m_scene->removeInstance(i);
			}
			//for (auto it = m_scene->getInstances().begin(); it != m_scene->getInstances().end(); it++)
			//{
			//	Instance* instance = *it;
			//	if (ImGui::TreeNode((void*)(intptr_t)i, "Object %d", i))
			//	{
			//		//ImGui::DragFloat3("Object Position", &instance->m_transform[3][0], 0.1f, -100.0f, 100.0f);
			//		ImGui::TreePop();
			//	}

			//	i++;
			//}
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Options"))
	{
		if (ImGui::TreeNode("Camera"))
		{
			ImGui::DragFloat("Camera Speed", &m_cameraSpeed, 0.1f, 0.1f, 100.0f);
			ImGui::Checkbox("Follow Animation", &followObjectAnim);

			ImGui::TreePop();
		}

		ImGui::TreePop();
	}

	ImGui::End();

	//for (int i = m_scene->getInstances().size() - 1; i >= 0; i--) 
	for (int i = 0; i < m_scene->getInstances().size(); i++)
	{
		m_scene->getInstances()[i]->m_position = { 0,i * 0.05f,0 };
		m_scene->getInstances()[i]->m_rotation = { 90 + ((objectIndex + i) * 2.5f),0,90 + ((objectIndex + i) * 2.5f) };
	}
	objectIndex--;

	if (followObjectAnim)
	{
		glm::vec3 camFollowPos = m_scene->getCamera()->getPosition();
		camFollowPos.y += 0.05f;
		m_scene->getCamera()->setPosition(camFollowPos);
	}




	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application3D::draw() {

	// wipe the screen to the background colour 
	clearScreen();

	// update perspective in case window resized 
	//m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
	//	getWindowWidth() / (float)getWindowHeight(),
	//	0.1f, 1000.f);

	glm::mat4 projectionMatrix = m_camera.getProjectionMatrix(getWindowWidth(), (float)getWindowHeight());
	glm::mat4 viewMatrix = m_camera.getViewMatrix();


	m_scene->draw();
	//m_spearInstance->draw(&m_camera, (float)getWindowWidth(),(float)getWindowHeight(), m_ambientLight, &m_light);


	////===prep for quad===

	//// bind shader 
	//m_texturedShader.bind();

	//// bind transform 
	//auto pvm = projectionMatrix * viewMatrix * m_quadTransform;
	//m_texturedShader.bindUniform("ProjectionViewModel", pvm);

	//// bind texture location 
	//m_texturedShader.bindUniform("diffuseTexture", 0);

	//// bind texture to specified location 
	//m_gridTexture.bind(0);

	//// draw quad 
	//m_quadMesh.draw();

	// draw 3D gizmos 
	Gizmos::draw(projectionMatrix * viewMatrix);

	// draw 2D gizmos using an orthogonal projection matrix 
	Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());
}

float Application3D::random(float min, float max)
{
	max *= 10;
	min *= 10;
	int total = ((int)floor(max - min) + 1);
	float randomOutput = (rand() % total); //ensure total would not reach above rand_max (32767)
	randomOutput += min;
	randomOutput /= 10;
	return randomOutput;
}
